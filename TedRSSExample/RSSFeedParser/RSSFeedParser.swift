//
//  RSSFeedParser.swift
//  TedRSSExample
//
//  Created by Андрей Огородников on 4/23/15.
//  Copyright (c) 2015 Andrei Ogorodnikov. All rights reserved.
//

import Foundation

private let _SingletonSharedInstance = RSSFeedParser()

class RSSFeedParser: NSObject, NSXMLParserDelegate {
    
    class var sharedInstance : RSSFeedParser { return _SingletonSharedInstance }
    
    var rssFeedItems = [RSSFeedItem]()
    var currentRssFeedItem: RSSFeedItem!
    var currentElement: String!
    
    func getRssFeedItems(getRssFeedItemsBlock:(Bool,[RSSFeedItem]) -> Void) {
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            let rssUrl = "http://www.ted.com/themes/rss/id/6"
            let xmlParser = NSXMLParser(contentsOfURL: NSURL(string: rssUrl))!
            xmlParser.delegate = self
            let success = xmlParser.parse()
            dispatch_async(dispatch_get_main_queue()) {
                getRssFeedItemsBlock(success,self.rssFeedItems)
            }
        }
    }
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [NSObject : AnyObject]) {
        switch elementName {
            case "item": currentRssFeedItem = RSSFeedItem()
            case "title": if currentRssFeedItem != nil { currentElement = "title" }
            case "media:content": if currentRssFeedItem != nil { currentRssFeedItem.videoLink = attributeDict["url"] as! String }
            default: break
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String?) {
        if currentElement == nil { return }
        switch currentElement {
            case "title": currentRssFeedItem.title += string!
            default: break
        }
    }
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        switch elementName {
            case "item":
                rssFeedItems.append(currentRssFeedItem)
                currentRssFeedItem = nil
            case "title": currentElement = nil
            default: break
        }
    }
   
}
