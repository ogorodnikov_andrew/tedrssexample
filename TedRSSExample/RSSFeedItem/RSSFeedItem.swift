//
//  RSSFeedItem.swift
//  TedRSSExample
//
//  Created by Андрей Огородников on 4/23/15.
//  Copyright (c) 2015 Andrei Ogorodnikov. All rights reserved.
//

import Foundation

class RSSFeedItem {
   
    var title:String = ""
    var videoLink:String = ""
    
}
