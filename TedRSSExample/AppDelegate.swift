//
//  AppDelegate.swift
//  TedRSSExample
//
//  Created by Андрей Огородников on 4/23/15.
//  Copyright (c) 2015 Andrei Ogorodnikov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.backgroundColor = UIColor.whiteColor()
        window?.rootViewController = UINavigationController(rootViewController: RSSFeedViewController())
        window?.makeKeyAndVisible()
        return true
    }
}

