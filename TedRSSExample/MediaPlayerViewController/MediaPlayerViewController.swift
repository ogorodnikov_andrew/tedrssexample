//
//  MediaPlayerViewController.swift
//  TedRSSExample
//
//  Created by Андрей Огородников on 4/24/15.
//  Copyright (c) 2015 Andrei Ogorodnikov. All rights reserved.
//

import UIKit
import MediaPlayer

class MediaPlayerViewController: UIViewController {

    var videoLink:String!
    var moviePlayer:MPMoviePlayerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        self.title = "Media Player"
        
        let url:NSURL = NSURL(string: videoLink)!
        moviePlayer = MPMoviePlayerController(contentURL: url)
        self.view.addSubview(moviePlayer.view)
        moviePlayer.view.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[player]|", options: NSLayoutFormatOptions.AlignAllLeft, metrics: nil, views: ["player" as NSString: moviePlayer.view ]))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[player]|", options: NSLayoutFormatOptions.AlignAllLeft, metrics: nil, views: ["player" as NSString: moviePlayer.view ]))
        moviePlayer.fullscreen = true
        moviePlayer.controlStyle = MPMovieControlStyle.Embedded
    }

}
