//
//  RSSFeedViewController.swift
//  TedRSSExample
//
//  Created by Андрей Огородников on 4/23/15.
//  Copyright (c) 2015 Andrei Ogorodnikov. All rights reserved.
//

import UIKit

class RSSFeedViewController: UITableViewController {

    let reuseIdentifier = "reuseIdentifier"
    
    var items:[RSSFeedItem]!
    
    var activityIndicator : UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "TED RSS"
        tableView.tableFooterView = UIView(frame: CGRectZero)
        tableView.registerNib(UINib(nibName: "RSSFeedCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 44;
        
        activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0,0, 32, 32)) as UIActivityIndicatorView
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        view.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        RSSFeedParser.sharedInstance.getRssFeedItems { (success:Bool, fetchedItems:[RSSFeedItem]) -> Void in
            self.activityIndicator.stopAnimating()
            if !success {
                //todo error
            } else {
                self.items = fetchedItems
                self.tableView.reloadData()
            }
        }
    }


    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (items == nil ? 0 : items.count)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! RSSFeedCell
        cell.titleLabel.text = items[indexPath.row].title
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let controller = MediaPlayerViewController()
        controller.videoLink = items[indexPath.row].videoLink
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
